import gvar as gv
import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, 'proton-mass-sigma')

# First checkout compatible branch of proton-mass-sigma project;
# this branch is guaranteed to be compatible with the code below
#   git clone --single-branch --branch chiral_dynamics_2021 https://gitlab.com/millernb-lqcd/proton-mass-sigma


import xpt


def main():

    input_output = xpt.io.InputOutput()
    data, ensembles = input_output.load_data(include_phys=False)
    data_phys_point = input_output.load_data_phys_point()
    prior = input_output.load_prior()

    plt.rcParams['figure.figsize']  = (6.75, 6.75/1.3)
    plt.rcParams['font.size']  = 25
    plt.rcParams['xtick.labelsize'] = 18
    plt.rcParams['ytick.labelsize'] = 18

    plot_m_n(data, data_phys_point, prior, ensembles)
    l4_sensitivity_plot(data, data_phys_point, prior, ensembles)
    plot_histograms(data, data_phys_point, prior, ensembles)
    #plot_data(data, data_phys_point, prior, ensembles)


def plot_m_n(data, data_phys_point, prior, ensembles):
    models = [
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt=None, order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False)
    ]
    mass_average = xpt.mass.MassAverage(data, data_phys_point, prior, models, ensembles)

    fig = mass_average.plot_fit(xparam='eps_pi', observable='eps_proton', xlabel='$\epsilon_\pi = m_\pi / 4 \pi F_\pi$')
    fig.savefig('figs/mn_vs_epspi.pdf', transparent=True, bbox_inches='tight')

    fig = mass_average.plot_fit(xparam='eps2_a', observable='eps_proton', xlabel='$\epsilon^2_a = (a/2w_0)^2$')
    fig.savefig('figs/mn_vs_eps2a.pdf', transparent=True, bbox_inches='tight')


def l4_sensitivity_plot(data, data_phys_point, prior, ensembles):

    models = [
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='lo', order_xpt=None, order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n4lo', order_xpt=None, order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt=None, order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt='lo', order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt='nlo', order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt='n2lo', order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
    ]

    results = {}
    results['observables'] = {obs : [] for obs in models[0].observables}
    results['q'] = []
    results['logGBF'] = []

    for mean in np.linspace(3.5, 5.5, 10):

        prior_new = {}
        prior_new.update(prior)
        prior_new['l_4'] = gv.gvar(mean, 0.1) 

        mass_average = xpt.mass.MassAverage(data, data_phys_point, prior_new, models, ensembles)
        for obs in results['observables']:
            results['observables'][obs].append(mass_average.extrapolate(observable=obs))
            #print(mass_average.fitter().extrapolate(observable=obs, debug=True))

    fig, ax = plt.subplots()

    pm = lambda g, k : gv.mean(g) + k*gv.sdev(g)

    x = np.linspace(3.5, 5.5, 10)
    ax.plot(
        x, 
        pm(results['observables']['sigma_p'], 0),
        ls='--',
        color='royalblue'
    )

    ax.plot(
        x, 
        pm(results['observables']['sigma_p'], -1),
        x, 
        pm(results['observables']['sigma_p'], 1), 
        color='royalblue'
    )

    ax.fill_between(
        x,
        pm(results['observables']['sigma_p'], 1),
        pm(results['observables']['sigma_p'], -1),
        color='royalblue',
        alpha=0.7
    )

    sigma_bmw = gv.gvar(37, 5)
    sigma_gupta = gv.gvar(61.6, 6.4)
    ax.axhline(pm(sigma_bmw, 0), color='mediumspringgreen', ls='--')
    ax.axhspan(pm(sigma_bmw, -1), pm(sigma_bmw, +1), alpha=0.3, color='mediumspringgreen')
    ax.text(3.5, pm(sigma_bmw, 0), 'BMW 2020')
    ax.axhline(pm(sigma_gupta, 0), color='mediumspringgreen', ls='--')
    ax.axhspan(pm(sigma_gupta, -1), pm(sigma_gupta, +1), alpha=0.3, color='mediumspringgreen')
    ax.text(3.5, pm(sigma_gupta, 0), 'Gupta 2021')

    l_4_flag = gv.gvar(4.73, 0.1)
    #ax.axvline(pm(l_4_flag, 0), color='grey', ls='--')
    #ax.axvspan(pm(l_4_flag, -1), pm(l_4_flag, +1), alpha=0.3, color='grey')

    ax.set_xlabel('$\overline \ell_4$')
    ax.set_ylabel('$\sigma_{N\pi}$')

    fig.savefig('figs/sigma_vs_l4_mean.pdf', transparent=True, bbox_inches='tight')


def plot_data(data, data_phys_point, prior, ensembles):
    models = [
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt=None, order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
    ]

    mass_average = xpt.mass.MassAverage(data, data_phys_point, prior, models, ensembles)

    fig= mass_average.plot_params(xparam='eps_pi', yparam='eps_proton', xlabel='$\epsilon^2_a = (a/2w_0)^2$')
    fig.tight_layout()
    fig.savefig('figs/mn_data.pdf', transparent=True, bbox_inches='tight')


def plot_histograms(data, data_phys_point, prior, ensembles):
    models = [
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt=None, order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt='lo', order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt='nlo', order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
        xpt.mass.MassModel(observables=['eps_proton', 'm_proton', 'sigma_p'], order_light='n2lo', order_xpt='n2lo', order_disc=None, order_strange=None, empirical_bayes=False, delta_xpt=False),
    ]

    mass_average = xpt.mass.MassAverage(data, data_phys_point, prior, models, ensembles)

    fig = mass_average.plot_histogram(compare='order_xpt')
    fig.savefig('figs/histogram_all.pdf', transparent=True, bbox_inches='tight')

    fig = mass_average.plot_histogram(observable='sigma_p', compare='order_xpt')
    fig.savefig('figs/histogram_sigma.pdf', transparent=True, bbox_inches='tight')


if __name__ == "__main__":
    main()