\documentclass[usenames,dvipsnames,aspectratio=169]{beamer}
\title{The nucleon mass and sigma term from lattice QCD}

\author[shortname]{
	\texorpdfstring{\textcolor{ProcessBlue}{Nolan Miller}}{} \and 
	G.~Bradley \and
	C.~Drischler \and
	D.~Howarth \and
	C.~K\"orber \and
	M.~Lazarow \and
	H.~Monge-Camacho \and
	A.~Meyer \and
	A.~Nicholson \and
	P.~Vranas \and
	A.~Walker-Loud \and
	\emph{others}
}
					  
\date{Nov 17, 2021}

\titlegraphic{
	\includegraphics[height=2cm]{figs/callat_logo.png}\hspace*{1cm}~%
	\includegraphics[height=2cm]{figs/doe_sc_logo.pdf}\hspace*{1cm}~%
	\includegraphics[height=2cm]{figs/unc_logo.png} %unc_logo
}

\usepackage{braket}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage{slashed}
\usepackage{cancel}

\usetheme{default}
\usecolortheme{dove}
\setbeamercolor{frametitle}{fg=RoyalPurple,bg=Orchid!20}

\setbeamertemplate{navigation symbols}{%
	\usebeamerfont{footline}%
	\usebeamercolor[fg]{footline}%
	\hspace{1em}%
	\insertframenumber
}
\addtobeamertemplate{frametitle}{
   \let\insertframesubtitle\insertsectionhead}{}
\setbeamertemplate{background}
{\includegraphics[width=\paperwidth,keepaspectratio]{figs/background.jpg}}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

%\begin{frame}
%	\frametitle{Outline}
%	\tableofcontents
%\end{frame}

\begin{frame}
	\frametitle{Really, the nucleon mass?}
	\begin{columns}
	\begin{column}{0.8\textwidth}
	The nucleon mass is one of the most precisely measured quantities in physics
	\begin{itemize}
		\item Experiment: relative uncertainty to $\sim 1$ part per 10 billion
		\item Lattice: relative uncertainty to $\sim 1$ part per 100
	\end{itemize}
	\vspace{\baselineskip}

	With the lattice, we can...
	\begin{itemize}
		\item ...check theory against experiment
		\item ...study convergence of heavy baryon $\chi$PT
		\item ...use $M_N$ to access other observables (eg, $\sigma_{N\pi}$)
	\end{itemize}	
	\end{column}
	\begin{column}{0.2\textwidth}
		\begin{figure}
			\includegraphics[width=\textwidth]{./figs/particle_properties.png}
			\caption*{\href{https://xkcd.com/1862/}{xkcd:1862}}
		\end{figure}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{The sigma terms: what are they and what are they good for?}
	\begin{columns}
	\begin{column}{0.5\textwidth}
	By definition, the sigma terms are the quark condensates inside the nucleon
	\begin{equation*}
		\sigma_{q} = m_{q}\braket{N | \overline q q | N}
	\end{equation*}

	These parameterize:
	\begin{itemize}
	\item the $q$-quark mass shift to $M_N$
	\item the coupling to the Higgs
	\item {\color{ProcessBlue} the spin-independent coupling to some dark matter candidates}
	\end{itemize}
	\end{column}

	\begin{column}{0.5\textwidth}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{./figs/lux.jpg}
		\caption*{Large Underground Xenon experiment}
	\end{figure}
	\end{column}
	\end{columns}

\end{frame}

\begin{frame}
	\frametitle{Phenomenological significance of the nucleon-pion sigma term}

	\begin{columns}
	\begin{column}{0.7\textwidth}

	Let $\chi$ be the lightest neutralino from the minimal supersymmetric extension to the Standard Model (MSSM). 
	\begin{equation*}
	\mathcal L_q = \sum_i \underbrace{\alpha_{3i} \,  \overline \chi \chi \, \overline q_i q_i}_\text{spin-independent}
	+ \sum_i \underbrace{\alpha_{2i} \, \overline \chi \gamma_\mu \gamma_5 \chi \, \overline q_i \gamma^\mu \gamma_5 q_i}_\text{spin-dependent}
	\end{equation*}

	MSSM direct dark matter experiments look for scattering off nuclei
	\begin{itemize}
	\item interactions either spin-independent ($\sigma_{q}$) or spin-dependent ($g_A^{q}$)
	\item spin-dependent cross section suppressed by $\beta^2 = (v/c)^2$
	\end{itemize}
	\end{column}
	\begin{column}{0.30\textwidth}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{./figs/neutralino_scattering.png}
			\caption*{[Thornberry; \href{https://doi.org/10.1140/epjs/s11734-021-00093-1}{doi:10.1140/epjs/s11734-021-00093-1}]}
		\end{figure}
	\end{column}
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{Two paths to the sigma term}
	
	\begin{columns}
	\begin{column}{0.5\textwidth}
	The direct approach:
	\begin{itemize}
	\item Generate $\sigma_{N\pi} = \hat m \braket{N | \overline u u + \overline d d | N}$ per lattice ensemble
	\item Fit the 3-point function
	\item Extrapolate $\sigma_{N\pi}$ to the physical point
	\end{itemize}
	\vfill
	\end{column}

	\begin{column}{0.5\textwidth}
	{\color{ProcessBlue} The Feynman-Hellman approach}:
	\begin{itemize}
	\item Generate $C(t) = \braket{0 | O_N^\dagger(t) O_N(0) | 0}$
	\item Fit the 2-point function
	\item Extrapolate $M_N$ to the physical point
	\item Calculate $\sigma_{N\pi} = \hat m \frac{\partial M_N}{\partial \hat m} \large \vert_\text{phys point}$
	\end{itemize}
	

	\end{column}
	\end{columns}
	\begin{figure}
		\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\textwidth]{./figs/connected_with_t.png}
		\end{subfigure}
		\hfill
		\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\textwidth]{./figs/2-pt_with_t.png}
		\end{subfigure}
		\caption*{[FLAG; \href{https://arxiv.org/abs/1902.08191}{arXiv:1902.08191}]}
	\end{figure}
	\vfill
\end{frame}

\begin{frame}
	\frametitle{Previous work}
	\begin{columns}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/flag_sigma.png}
			\caption*{[FLAG, 2019; \href{https://arxiv.org/abs/1902.08191}{arXiv:1902.08191}]}
		\end{figure}
		\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/gupta_sigma.png}
			\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
		\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Project objectives \& lattice details}

	\begin{columns}
		\begin{column}{0.4\textwidth}
		Objectives:
		\begin{enumerate}
		\item Fit correlators
		\item Extrapolate masses to the phys point
		\item Calculate $\sigma_{N\pi}$ via the Feynman-Hellman theorem
		\end{enumerate}
		\begin{table}[]
			\begin{tabular}{|l|l|}
			\hline
			Action         & \begin{tabular}[c]{@{}l@{}}Valence: Domain-wall\\ Sea: staggered\end{tabular} \\ \hline
			Gauge configs  & MILC -- thanks!                                                               \\ \hline
			$m_\pi$        & 130 - 400 MeV                                                                 \\ \hline
			$a$            & 0.06 - 0.15 fm                                                                \\ \hline
			Scale setting? & Done!                                                                         \\ \hline
			\end{tabular}
		\end{table}

		\end{column}

		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/ensembles_data.png}
				%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
			\end{figure}
			\begin{figure}
				\includegraphics[width=0.5\textwidth]{./figs/milc_cow.png}
				%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{$N$ correlator fits (\texttt{a09m135})}

	\begin{columns}
		\begin{column}{0.35\textwidth}
		\begin{figure}
			\includegraphics[width=1.1\textwidth]{./figs/nucleon_a09m135_eff_mass.pdf}
		\end{figure}
		\begin{align*}
			C(t) &= \sum_n A_n e^{-E_n t} \\
			\implies m_\text{eff}(t) &= \log\frac{C(t)}{C(t+1)}
		\end{align*}
		\end{column}
		\begin{column}{0.65\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/nucleon_a09m135_stability.pdf}
		\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Fit strategy: mass formulae}
	Instead of fitting $M_N$, fit dimensionless $M_N/\Lambda_\chi$ $\qquad (\Lambda_\chi = 4 \pi F_\pi\,, \,\,\epsilon_\pi = m_\pi / \Lambda_\chi)$
	\begin{align*}
		\frac{M_N}{{\color{JungleGreen}4 \pi F_\pi}} = &\phantom{+}  c_0
		& \text{(LLO)}\\
		&+ \left(\beta^\text{(2)}_N - {\color{JungleGreen} c_0 \overline \ell_4^r} \right) \epsilon_\pi^2 
		+ {\color{JungleGreen} c_0 \epsilon_\pi^2 \log \epsilon_\pi^2} 
		& \text{(LO)} \nonumber \\
		&- \frac{3\pi}{2} g_{\pi NN}^2  \epsilon_\pi^3
		& \text{(NLO)} \nonumber\\
		& + \left( \beta_{N}^{(4)} + {\color{JungleGreen} c_0 \left(\overline \ell_4^r\right)^2} - {\color{JungleGreen} c_0 \beta_{F}^{(4)}}  \right) \epsilon_\pi^4
		&\text{(N$^2$LO)} \\
		&\qquad - {\color{JungleGreen}\frac 14 c_0 \epsilon_\pi^4  \left(\log  \epsilon^2 \right)^2} 
		+ \left( \alpha_N^\text{(4)} - {\color{JungleGreen} c_0 \alpha_F^\text{(4)}} - {\color{JungleGreen} 2 c_0 \overline \ell_4^r} \right) \epsilon_\pi^4 \log{\epsilon_\pi^2} 
		\nonumber
	  \end{align*}
	%Some observations:
	\begin{itemize}
		\item The {\color{JungleGreen} $1/4 \pi F_\pi$} expansion doesn't \emph{require} fitting additional LECs; it only adds some log terms
		\item We'd like to push this $M_N/\Lambda_\chi$ analysis as far as possible
	\end{itemize}
\end{frame}

\begin{frame}	
	\frametitle{$M_N/\Lambda_\chi$ extrapolations}

	\begin{columns}
		\begin{column}{0.50\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/mn_vs_epspi.pdf}
				%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
			\end{figure}
			\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/mn_vs_eps2a.pdf}
			%\caption*{Blank}
		\end{figure}
		\end{column}
	\end{columns}
	\begin{itemize}
		\item Fit has negligible lattice spacing dependence
	\end{itemize}

\end{frame}

\begin{frame}	
	\frametitle{Expansion of $\sigma_{N\pi}$}
	%Expand $\sigma_{N\pi} = \hat m \frac{\partial M_N}{\partial \hat m} \approx m_\pi^2 \frac{\partial M_N}{\partial m_\pi^2} \approx \frac 12 \epsilon_\pi \frac{\partial M_N}{\partial \epsilon_\pi}$ where $\epsilon_\pi = m_\pi / \Lambda_\chi = m_\pi / 4 \pi F_\pi$ 
	Expand $\sigma_{N\pi} = \hat m \frac{\partial M_N}{\partial \hat m} \rightarrow \hat m \frac{\partial}{\partial \hat m} = \frac 12 \epsilon_\pi (\cdots) \frac{\partial}{\partial \epsilon_\pi}$
	\begin{equation*}
	\sigma_{N\pi} = \frac 12 \epsilon_\pi \left[
		1 
		+ \epsilon_\pi^2 \left( \frac 52 - \frac 12 \overline \ell_3 - 2 \overline \ell_4 \right) 
		+ \mathcal{O} \left(\epsilon_\pi^3\right)
	\right]
	\overbrace{
	\left[
		{\color{ProcessBlue} \Lambda_\chi^* \frac{\partial \left(M_N/\Lambda_\chi\right)}{\partial \epsilon_\pi}}
		+ {\color{JungleGreen}\frac{M_N^*}{\Lambda_\chi^*} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi}}
	\right]
	}^{\frac{\partial M_N}{\partial \epsilon_\pi}}
	\end{equation*}

	\begin{align*}
	\frac 12 \epsilon_\pi {\color{ProcessBlue} \Lambda_\chi^* \frac{\partial \left(M_N/\Lambda_\chi\right)}{\partial \epsilon_\pi}} &=
	\frac 12 \Lambda_\chi^* \left[
		\left({\color{RubineRed} -2 c_0 \left(\overline \ell_4  - 1 \right)}  + 2 \beta^\text{(2)}_N \right) \epsilon_\pi^2 
		+ \mathcal{O}\left(\epsilon_\pi^3\right)
	\right] 
	\sim 10 \text{ MeV} 
	\\
	\frac 12 \epsilon_\pi {\color{JungleGreen}\frac{M_N^*}{\Lambda_\chi^*} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi}} &=
	\frac 12 M_N^* \left[ {\color{RubineRed} 2 \left(\overline \ell_4  - 1 \right)\epsilon_\pi^2 } 
	+ \mathcal{O}\left(\epsilon_\pi^3\right) \right]
	\sim 40 \text{ MeV}
	\end{align*}
	\begin{itemize}
		\item Fitting {\color{ProcessBlue} $M_N/\Lambda_\chi	$} requires an {\color{JungleGreen} extra term}
		\item Largest contribution comes from {\color{JungleGreen} second term} $\implies$ $\overline \ell_4$ must be precisely determined
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Comparing $\chi$PT terms by order}

	\begin{figure}
		\includegraphics[width=1.0\textwidth]{./figs/histogram_all.pdf}
		%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
	\end{figure}
	Here we use:
	\begin{itemize}
		\item $F_\pi$-derived $\chi$PT terms up to $\mathcal{O}(\epsilon_\pi^2)$ \& $M_N$-derived $\chi$PT terms up to $\mathcal{O}(\epsilon_\pi^4)$ 
		\item FLAG average for $\overline \ell_4$
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{$\sigma_{N\pi}$ as a function of $\overline \ell_4$}

	\begin{columns}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/flag_l4.png}
			\caption*{[FLAG, 2019; \href{https://arxiv.org/abs/1902.08191}{arXiv:1902.08191}]}
		\end{figure}
		\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/sigma_vs_l4_mean.pdf}
			%\caption*{Blank}
		\end{figure}
		\end{column}
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{$F_\pi$ extrapolation: $\mathcal{O}(\epsilon_\pi^2)$ $\chi$PT + $\mathcal{O}(a^4)$}

	\begin{columns}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/fpi_vs_epi.pdf}
			%\caption*{Blank}
		\end{figure}
		\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/l_4_vs_epsa.pdf}
			%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
		\end{figure}
		\end{column}
	\end{columns}

	\begin{equation*}
		F_\pi = F_0 \left[
			1 
			+ \epsilon_\pi^2 \left( \overline \ell_4^r\left(\mu = 4 \pi F_\pi \right) -\log \epsilon_\pi^2  \right)
			+ \mathcal{O}\left(\epsilon_\pi^4 \right)
		\right]
	\end{equation*}
	\begin{equation*}
		\overline \ell_4^r(\mu) = (4 \pi)^2 \ell_4^r(\mu) \qquad \overline \ell_4 = \overline \ell_4^r(\mu) - \log\left[\frac{\left(m_\pi^*\right)^2}{\mu^2 }\right]
	\end{equation*}

\end{frame}

\begin{frame}
	\frametitle{Summary \& future work}

	\begin{columns}
		\begin{column}{0.5\textwidth}
		In conclusion:
		\begin{itemize}
			\item Tension exists between phenomenology and the lattice w.r.t. $\sigma_{N\pi}$
			\item Can extract $\sigma_{N\pi}$ from a dimensionless fit of $M_N/\Lambda_\chi$
			\item However, this requires a precise determination of the LECs associated with the chiral expression for $F_\pi$
		\end{itemize}
		\vspace{\baselineskip}

		To do:
		\begin{itemize}
			\item Carefully determine $F_\pi$ LECs for $\sigma_{N\pi}$
			\item Add FV corrections
		\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/histogram_sigma.pdf}
				%\caption*{Some fig}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\section{Extra slides}

\begin{frame}
	\frametitle{Mass formula with $\Delta$}
	\begin{align*}
		\frac{M_N}{{\color{JungleGreen}4 \pi F_\pi}} = &\phantom{+}  c_0
		& \text{(LLO)}\\
		&+ \left(\beta^\text{(2)}_N - {\color{JungleGreen} c_0 \overline \ell_4^r} \right) \epsilon_\pi^2 
		& \text{(LO)} \nonumber \\
		&- \frac{3\pi}{2} g_{\pi NN}^2  \epsilon_\pi^3
		- {\color{RubineRed} \frac{4}{3} g_{\pi N\Delta}^2 \mathcal{F}(\epsilon_\pi, \epsilon_{N\Delta}, \mu)}
		& \text{(NLO)} \nonumber\\
		& + {\color{RubineRed}\gamma_N^\text{(4)} \epsilon_\pi^2 \mathcal{J} (\epsilon_\pi, \epsilon_{N \Delta}, \mu)}
		- {\color{JungleGreen}\frac 14 c_0 \epsilon_\pi^4  \left(\log  \epsilon^2 \right)^2}
		&\text{(N$^2$LO)} \nonumber \\
		&\qquad + \left( \alpha_N^\text{(4)} - {\color{JungleGreen} c_0 \alpha_F^\text{(4)}} - {\color{JungleGreen} 2 c_0 \overline \ell_4^r} \right) 
		\epsilon_\pi^4 \log{\epsilon_\pi^2} 
		\nonumber \\ 
		&\qquad 
		+ \left( \beta_{p}^{(4)} + {\color{JungleGreen} c_0 \left(\overline \ell_4^r\right)^2} - {\color{JungleGreen} c_0 \beta_{F}^{(4)}}  \right) \epsilon_\pi^4 \nonumber
	  \end{align*}
	Some observations:
	\begin{itemize}
		\item The {\color{JungleGreen} $1/4 \pi F_\pi$} expansion doesn't \emph{require} fitting additional LECs
		\item The {\color{RubineRed} $\Delta$-$\chi$PT} terms push the fit downwards
	\end{itemize}
\end{frame}

\end{document}
