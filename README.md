# proton-sigma-talk

Talk for work on proton mass and sigma term.

Link to slides:

- https://millernb.gitlab.io/proton-sigma-talk/presentation.pdf

## Abstract

We report a preliminary, percent-level determination of the nucleon mass $`M_N`$ and a roughly 5%-level determination of the sigma term $`\sigma_{\pi N}`$ from lattice QCD. We find that our $`M_N`$ extrapolation to the physical point agrees with the PDG average. Next we review the significance of $`\sigma_{\pi N}`$ for direct dark matter searches, and we explore the sensitivity of this observable over choice of chiral models. For our lattice calculations, we employ Möbius domain wall fermions on $`N_F = 2 + 1+ 1`$ dynamically, highly-improved staggered quark fermions. We include five pion masses, ranging from 130 MeV to 350 MeV; four lattice spacings, ranging from 0.06 fm to 0.15 fm; and multiple lattice volumes.